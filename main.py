#region Imports
import pickle
from genericpath import isfile
from cocos.scene import Scene
from cocos.director import director
from menu import GameMenu
from model import GameModel
#endregion

window = director.init(width=640, height=1024, caption="Tic-Tac-Toe", resizable=True)

if isfile('model.inf'):
    print('loaded')
    with open('model.inf', 'rb') as f:
        model = pickle.load(f)
    print(model)
else:
    model = GameModel('model.inf')
print(model.used_board)

menu_scene = Scene(GameMenu(model))

director.run(menu_scene)
