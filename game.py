import random
import pyglet
from cocos.actions import RotateBy, ScaleTo

from cocos.director import director
from cocos.draw import Line
from cocos.layer import Layer
from cocos.particle_systems import Explosion
from cocos.sprite import Sprite
from cocos.text import Label

from model import GameModel

enter_anim = RotateBy(360, 0.5) | ScaleTo(0.25, 0.5)
IMAGES = {
    'board': pyglet.resource.image('board.png'),
    'X': pyglet.resource.image('symbol-X.png'),
    'O': pyglet.resource.image('symbol-O.png')
}
corners = (0, 0), (0, 2), (2, 0), (2, 2)
edges = (0, 1), (1, 0), (1, 2), (2, 1)
winning_spots = [
    ((0, 0), (1, 1), (2, 2)), # diag
    ((0, 2), (1, 1), (2, 0)), # diag
    ((0, 0), (1, 0), (2, 0)), # vert
    ((0, 1), (1, 1), (2, 1)), # vert
    ((0, 2), (1, 2), (2, 2)), # vert
    ((0, 0), (0, 1), (0, 2)), # hori
    ((1, 0), (1, 1), (1, 2)), # hori
    ((2, 0), (2, 1), (2, 2)), # hori
]


class TitleLayer(Layer):
    def __init__(self):
        super(TitleLayer, self).__init__()

        label = Label('Tic-Tac-Toe', font_name='Times New Roman', font_size=32, anchor_x='center', anchor_y='center')
        # set the label in the center of the screen
        label.position = 320, 1000
        self.add(label)


class BoardLayer(Layer):
    is_event_handler = True

    def __init__(self, model):
        super(BoardLayer, self).__init__()
        self.model = model # type: GameModel
        print(self.model.opponent)
        print(self.model.difficulty)
        self.marks = ['X', 'O']
        self.sprites = []  # list of sprites so that they can be removed after a game has finished
        self.board = Sprite(IMAGES['board'], position=(320, 450))
        self.add(self.board)
        self.cpu = {
            'Easy': self.easy_move,
            'Medium': self.medium_move,
            'Hard': self.hard_move
        }

        if self.model.playing:
            self.load_game()
        else:
           self.model.playing = True

    def load_game(self):
        for y, row in enumerate(self.model.used_board):
            for x, mark in enumerate(row):
                if mark not in {'X', 'O'}:
                    continue
                new_sprite = Sprite(IMAGES[mark], scale=0, position=self.model.piece_locations[y][x])
                self.sprites.append(new_sprite)
                self.add(new_sprite)
                new_sprite.do(enter_anim)

    def on_mouse_press(self, x, y, buttons, modifiers):
        # check if the click was within the board
        if not self.model.playing:
            return
        x, y = director.get_virtual_coordinates(x, y)
        if self.board.position[0] - self.board.width/2 < x < self.board.position[0] + self.board.width/2:
            if self.board.position[1] - self.board.height/2 < y < self.board.position[1] + self.board.height/2:
                # find the pythonic coordinate of the click on the board
                clicked_x = int((x-20)/200)
                clicked_y = abs(int((y-150)/200)-2)  # subtract two to get pythonic coordinates
                if self.model.used_board[clicked_y][clicked_x] in {'X', 'O'}:
                    return
                move = self.marks[self.model.move_count % 2]
                self.model.used_board[clicked_y][clicked_x] = move
                self.tile_clicked(clicked_y, clicked_x, move)
                if not self.check_for_win(clicked_y, clicked_x, move):
                    if len(self.possible_moves()) <= 0:
                        self.tie_game()
                    elif self.model.opponent == 'CPU':
                        self.cpu[self.model.difficulty]()

    def tile_clicked(self, y, x, mark):
        new_sprite = Sprite(IMAGES[mark], scale=0, position=self.model.piece_locations[y][x])
        self.sprites.append(new_sprite)
        self.add(new_sprite)
        new_sprite.do(enter_anim)
        self.model.move_count += 1
        self.model.save()

    def cpu_move(self, position):
        move = self.marks[self.model.move_count % 2]
        self.model.used_board[position[0]][position[1]] = move
        self.tile_clicked(position[0], position[1], move)
        self.check_for_win(position[0], position[1], move)

    def easy_move(self):
        possible = self.possible_moves()
        if len(possible) == 0:  # game over
            return
        choice = possible[random.randint(0, len(possible)-1)]
        self.cpu_move(choice)

    def medium_move(self):
        # take the center if not already taken
        if len(self.possible_moves()) == 0:
            return

        for combo in winning_spots:
            offense = self.check_board(combo, 'O')
            if offense:
                return self.cpu_move(offense)
        # defend if needed
        for combo in winning_spots:
            defense = self.check_board(combo, 'X')
            if defense:
                return self.cpu_move(defense)
        if len(self.possible_corners()) > 0:
            return self.cpu_move(random.choice(self.possible_corners()))
        else: return self.cpu_move(random.choice(self.possible_moves()))


    def hard_move(self):
        # take the center if not already taken
        if len(self.possible_moves()) == 0:
            return
        if self.model.move_count == 1:
            if self.model.used_board[1][1] != 'X':
                return self.cpu_move((1, 1))
            return self.cpu_move(random.choice(self.possible_corners()))
        # check for a winning spot
        for combo in winning_spots:
            offense = self.check_board(combo, 'O')
            if offense:
                return self.cpu_move(offense)
        # defend if needed
        for combo in winning_spots:
            defense = self.check_board(combo, 'X')
            if defense:
                return self.cpu_move(defense)
        if self.model.move_count == 3 and self.model.used_board[1][1] == 'X' and len(self.possible_corners()) > 0:
            return self.cpu_move(random.choice(self.possible_corners()))
        print('human edges: ' + str(self.num_human_edges()))
        print('open corners: ' + str(len(self.possible_corners())))
        if self.num_human_edges() > 0 and len(self.possible_corners()) > 0:
            print('should do a corner')
            return self.cpu_move(random.choice(self.possible_corners()))
        if len(self.possible_edges()) > 0:
            return self.cpu_move(random.choice(self.possible_edges()))
        if len(self.possible_corners()) > 0:
            return self.cpu_move(random.choice(self.possible_corners()))



    def check_board(self, win_combo, desired):
        status = {'X': [], 'O': [], 'open': []}
        for pos in win_combo:
            mark = self.model.used_board[pos[0]][pos[1]]
            if mark in {'X', 'O'}:
                status[mark].append(mark)
            else:
                status['open'].append(mark)
        if len(status[desired]) == 2 and len(status['open']) > 0:
            return status['open'][0]
        return None

    def num_human_edges(self):
        ret = []
        for y, row in enumerate(self.model.used_board):
            for x, spot in enumerate(row):
                print(spot)
                if spot == 'X':
                    print('found X at ' + str(y) + ' ' + str(x))
                    if (y, x) in edges:
                        ret.append(spot)
        return len(ret)

    def possible_edges(self):
        return [spot for row in self.model.used_board for spot in row if spot not in {'X', 'O'} and spot in edges]

    def possible_corners(self):
        return [spot for row in self.model.used_board for spot in row if spot not in {'X', 'O'} and spot in corners]

    def possible_moves(self):
        return [spot for row in self.model.used_board for spot in row if spot not in {'X', 'O'}]

    # local helper function
    def check_for_win(self, y, x, mark):
        if self.model.used_board[y][0] == self.model.used_board[y][1] == self.model.used_board[y][2] == mark:
            position = self.model.piece_locations[y][0], self.model.piece_locations[y][2]
        elif self.model.used_board[0][x] == self.model.used_board[1][x] == self.model.used_board[2][x] == mark:
            position = self.model.piece_locations[0][x], self.model.piece_locations[2][x]
        # check if previous move was on the main diagonal and caused a win
        elif self.model.used_board[0][0] == self.model.used_board[1][1] == self.model.used_board[2][2] == mark:
            position = self.model.piece_locations[0][0], self.model.piece_locations[2][2]
        # check if previous move was on the secondary diagonal and caused a win
        elif self.model.used_board[0][2] == self.model.used_board[1][1] == self.model.used_board[2][0] == mark:
            position = self.model.piece_locations[0][2], self.model.piece_locations[2][0]
        else:
            return False
        self.player_won(mark, position)
        return True

    def player_won(self, marker, line):
        self.model.playing = False
        self.model.reset_game()
        self.model.save()

        label = Label(marker + ' Won!', font_name='Times New Roman', font_size=50, anchor_x='center', anchor_y='center')
        # set the label in the center of the screen
        label.position = 320, 900
        self.sprites.append(label)
        self.add(label)

        new_line = Line(line[0], line[1], (0, 128, 0, 255), stroke_width=15)
        self.sprites.append(new_line)
        self.add(new_line)

        effect = Explosion()
        effect.auto_remove_on_finish = True
        effect.position = (320, 900)
        self.add(effect)

        from menu import PlayAgainMenu
        play_again = PlayAgainMenu(self)
        self.sprites.append(play_again)
        self.add(play_again)

    def tie_game(self):
        self.model.playing = False
        label = Label('Tie Game!', font_name='Times New Roman', font_size=50, anchor_x='center', anchor_y='center')
        # set the label in the center of the screen
        label.position = 320, 900
        self.sprites.append(label)
        self.add(label)

        from menu import PlayAgainMenu
        play_again = PlayAgainMenu(self)
        self.sprites.append(play_again)
        self.add(play_again)

    def reset_scene(self):
        for elem in self.sprites:
            self.remove(elem)
        self.sprites = []
        self.model.reset_game()
        self.model.playing = True