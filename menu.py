#region Imports
import cocos
import pyglet
import random

from cocos.menu import *
from cocos.director import director
from cocos.scene import Scene
from cocos.scenes.transitions import *
from pyglet.window import key
from collections import OrderedDict
from model import GameModel

#endregion

OPPONENTS = ['CPU', 'Player']
DIFFICULTIES = ['Easy', 'Medium', 'Hard']
BACK_IMG = pyglet.resource.image("left-arrow.png")
REPLAY_IMG = pyglet.resource.image("rotating-arrow.png")
TRANSITIONS = (
    RotoZoomTransition, JumpZoomTransition, MoveInBTransition, MoveInLTransition, MoveInRTransition,
    MoveInTTransition, SlideInBTransition, SlideInLTransition, SlideInRTransition, SlideInTTransition,
    FlipAngular3DTransition, FlipX3DTransition, FlipY3DTransition, ShuffleTransition, TurnOffTilesTransition,
    FadeTRTransition, FadeBLTransition, FadeUpTransition, FadeDownTransition,
    ShrinkGrowTransition, CornerMoveTransition, EnvelopeTransition, SplitColsTransition,
    SplitRowsTransition, FadeTransition, ZoomTransition
)

class MyMultipleMenuItem(MenuItem):
    def __init__(self, label, callback_func, items, default_item=0):
        self.my_label = label
        self.items = items
        self.idx = default_item
        if 0 > self.idx >= len(self.items):
            raise Exception("Index out of bounds")
        super(MyMultipleMenuItem, self).__init__(self._get_label(), callback_func)

    def _get_label(self):
        return self.my_label + self.items[self.idx]

    def on_key_press(self, symbol, modifiers):
        if symbol == key.LEFT:
            self.idx = (self.idx-1) % len(self.items)
        elif symbol in (key.RIGHT, key.ENTER):
            self.idx = (self.idx+1) % len(self.items)
        else:
            return False
        #if symbol in (key.LEFT, key.RIGHT, key.ENTER):
        self.item.text = self._get_label()
        self.item_selected.text = self._get_label()
        self.callback_func(self.idx)
        return True


class GameMenu(Menu):
    #region Constructor
    def __init__(self, model):
        # When we initialize the super class, we can to pass in a title for our awesome menu, that will be displayed above it
        super(GameMenu, self).__init__("Options")
        self.model = model  # type: GameModel

        self.menu_items = OrderedDict({
            'play': MenuItem("Play", self.play_game),
            'continue': MenuItem("Continue", self.start_scene), # must have this to maintain order
            'opponent': MyMultipleMenuItem("VS: ", self.change_opponent, OPPONENTS),
            'difficulty': MyMultipleMenuItem("Difficulty: ", self.change_difficulty, DIFFICULTIES),
            'exit': MenuItem("Exit", self.on_quit)
        })

        if not self.model.playing:
            print('not continue')
            self.model.set_opponent(OPPONENTS[0])
            self.model.set_difficulty(DIFFICULTIES[0])
            del self.menu_items['continue']

        self.create_menu(self.menu_items.values(), shake(), shake_back())
    #endregion

    #region Menu Event Handlers
    def play_game(self):
        print(self.model.opponent)
        self.model.reset_game()
        self.model.playing = False
        self.start_scene()

    def start_scene(self):
        from game import BoardLayer, TitleLayer
        print('continue')
        main_scene = cocos.scene.Scene(TitleLayer(), BoardLayer(self.model))
        trans = random.choice(TRANSITIONS)
        director.replace(trans(main_scene))
        #director.push(main_scene)

    def change_opponent(self, idx):
        self.model.set_opponent(OPPONENTS[idx])
        if OPPONENTS[idx] != 'CPU':
            self.menu_items['difficulty'].do(cocos.actions.ScaleTo(0, 0.25)) # | cocos.actions.MoveBy((0, -100), 0.25))
        elif OPPONENTS[idx] == 'CPU':
            self.menu_items['difficulty'].do(cocos.actions.ScaleTo(1, 0.25)) # | cocos.actions.MoveBy((0, 100), 0.25))

    def change_difficulty(self, idx):
        self.model.set_difficulty(DIFFICULTIES[idx])

    def on_quit(self):
        pyglet.app.exit()
    #endregion

class PlayAgainMenu(Menu):
    def __init__(self, board):
        super(PlayAgainMenu, self).__init__()

        self.board_layer = board # type: BoardLayer
        """Icon is made by roundicons from www.flaticon.com and is licensed by Creative Commons BY 3.0"""
        self.menu_items = OrderedDict({
            'back': ImageMenuItem(BACK_IMG, self.back),
            'play': ImageMenuItem(REPLAY_IMG, self.play) # must have this to maintain order
        })
        self.font_item['font_size'] = 90
        self.font_item_selected['font_size'] = 100
        positions = [(250, 800),(370, 800)]
        self.create_menu(self.menu_items.values(), shake(), shake_back(),
                         layout_strategy=fixedPositionMenuLayout(positions))
        #self.create_menu(self.menu_items.values(), shake(), shake_back())

    def back(self):
        self.board_layer.model.playing = False
        trans = random.choice(TRANSITIONS)
        director.replace(trans(Scene(GameMenu(self.board_layer.model))))
        #director.pop()

    def play(self):
        self.board_layer.reset_scene()