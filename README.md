# Cocos2d Tic-Tac-Toe
A 2D game make with cocos2d for CPS499-12

A native mac app build can be found here: https://www.dropbox.com/sh/fepwhu441wf7gb8/AAAyrBkFXtfaakiv7QlOOSpTa?dl=1

Attributions for sprite images used by filename:

The following sprites from flaticon.com have Free Licenses with attribution (https://file000.flaticon.com/downloads/license/license.pdf)
symbol-X.png - made by GraphicsBay
symbol-O.png - made by GraphicsBay
left-arrow.png - made by Roundicons
rotating-arrow.png - made by Becris

The following sprites from commons.wikimedia.org are shared under the Creative Commons Attribution-Share Alike 4.0 International (https://creativecommons.org/licenses/by-sa/4.0/deed.en)
board.png - made by Jacob Belanger



Instructions for play:
  - There are two modes, Player vs. Player and Player vs. Computer.
  - For PVP mode, simply take turns tapping on squares in order to complete a move.
  - For PVC mode, there are three difficulties [Easy, Medium, Hard] that can be toggled in the home menu page. 
  - If a game is not completed, the progress will be saved and a Continue button will be presented in the home menu screen at next launch.
  - After a game is completed, you can return to the menu screen by tapping the back button, or play anther game on the same settings using the replay button.

