import copy
import pickle

#region Constants
USED_BOARD = [
    [(0, 0), (0, 1), (0, 2)],
    [(1, 0), (1, 1), (1, 2)],
    [(2, 0), (2, 1), (2, 2)]
]

PIECE_LOCATIONS = [
    [(120, 650), (320, 650), (520, 650)],
    [(120, 450), (320, 450), (520, 450)],
    [(120, 250), (320, 250), (520, 250)]
]
#endregion


class GameModel(object):
    def __init__(self, file_path):
        self.file_path = file_path
        self.used_board = copy.deepcopy(USED_BOARD)
        self.piece_locations = copy.deepcopy(PIECE_LOCATIONS)
        self.number_of_wins = 0
        self.move_count = 0
        self.playing = False
        self.opponent = None
        self.difficulty = None

    def win(self):
        self.number_of_wins += 1

    def set_difficulty(self, diff):
        self.difficulty = diff

    def set_opponent(self, opp):
        self.opponent = opp

    def reset_game(self):
        self.used_board = copy.deepcopy(USED_BOARD)
        self.piece_locations = copy.deepcopy(PIECE_LOCATIONS)
        self.move_count = 0
        self.playing = False

    def save(self):
        print(self.used_board)
        print('playing: ' + str(self.playing))
        print('saved')
        with open(self.file_path, 'wb') as f:
            pickle.dump(self, f)